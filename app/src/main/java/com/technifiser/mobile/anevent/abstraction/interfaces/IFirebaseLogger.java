package com.technifiser.mobile.anevent.abstraction.interfaces;

import android.os.Bundle;

import com.technifiser.framework.uds.api.FirebaseLogger;

/**
 * Firebase analytics logger contract definition
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */

public interface IFirebaseLogger {

    /**
     * Get Firebase instance logger
     *
     * @return FirebaseLogger instance
     */
    FirebaseLogger getFirebaseLogger();

    /**
     * Report event using FirebaseAnalytics
     *
     * @param eventType String event Type
     * @param args      Bundle Args
     */
    void reportEvent(String eventType, Bundle args);
}
