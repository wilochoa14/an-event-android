package com.technifiser.mobile.anevent.core.models;

import java.io.Serializable;

/**
 * @author Luis Hernandez
 * @version 0.0.1
 */

public class Notifications implements Serializable{

    public String date;
    public String name;
    public String time;
    public String location;
    public String imageUri;
}
