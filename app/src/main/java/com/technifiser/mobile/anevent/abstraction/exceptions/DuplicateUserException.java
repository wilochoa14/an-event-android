package com.technifiser.mobile.anevent.abstraction.exceptions;

/**
 * @author Luis Hernandez
 * @version 0.0.1
 */

public class DuplicateUserException extends Exception{
    public DuplicateUserException(String response) {
        super(response);
    }
}
