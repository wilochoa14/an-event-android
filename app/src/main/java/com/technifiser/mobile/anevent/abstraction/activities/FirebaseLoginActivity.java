package com.technifiser.mobile.anevent.abstraction.activities;

import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.technifiser.framework.uds.api.FirebaseLogger;
import com.technifiser.framework.uds.api.activities.CommonLoginActivity;
import com.technifiser.framework.uds.api.util.AppSettings;
import com.technifiser.mobile.anevent.R;
import com.technifiser.mobile.anevent.abstraction.interfaces.IFirebaseLogger;
import com.technifiser.mobile.anevent.core.Application;
import com.technifiser.mobile.anevent.core.util.DisplayUtil;
import com.technifiser.mobile.anevent.core.view.Label;

/**
 * Firebase Login Activity custom abstraction
 *
 * @author Francisco Vásquez
 * @version 0.0.0
 */

public abstract class FirebaseLoginActivity<T>
        extends CommonLoginActivity<T> implements IFirebaseLogger {

    private CharSequence mTitle;

    private Toolbar toolbar;
    private AuthenticationType authType = AuthenticationType.LOGIN;

    @Override
    protected void onStart() {
        super.onStart();
        if (getFirebaseLogger() != null)
            getFirebaseLogger().reportAppOpen();
    }

    @Override
    public void setAccountAuthenticatorResult(Bundle result) {
        super.setAccountAuthenticatorResult(result);
        if (result == null)
            return;
        if (!result.containsKey(AppSettings.AUTH_EMAIL_ADDRESS))
            return;
        // log new user using firebase analytics
        Bundle eventArgs = new Bundle();
        eventArgs.putString(FirebaseAnalytics.Param.ITEM_ID, AppSettings.FIREBASE_NEW_USER_KEY);
        eventArgs.putString(FirebaseAnalytics.Param.VALUE, result.getString(AppSettings.AUTH_EMAIL_ADDRESS));
        getFirebaseLogger().getInstance().logEvent(authType.eventType, eventArgs);
    }

    @Override
    public FirebaseLogger getFirebaseLogger() {
        Application app = (Application) this.getApplicationContext();
        return app != null ? app.getFirebaseLogger() : null;
    }

    @Override
    public void reportEvent(String eventType, Bundle args) {
        if (getFirebaseLogger() == null)
            return;
        getFirebaseLogger().getInstance().logEvent(eventType, args);
    }

    public void setAuthenticationType(AuthenticationType authType) {
        this.authType = authType;
    }

    public enum AuthenticationType {

        SIGN_UP(FirebaseAnalytics.Event.SIGN_UP),
        LOGIN(FirebaseAnalytics.Event.LOGIN);

        public String eventType;

        AuthenticationType(String eventType) {
            this.eventType = eventType;
        }
    }

    /**
     * Set toolbar action bar
     *
     * @param toolbar support v7 Toolbar
     */
    public void setToolbar(@Nullable Toolbar toolbar) {
        if (toolbar == null)
            this.toolbar = (Toolbar) findViewById(getToolbarId());
        else
            this.toolbar = toolbar;
        if (this.toolbar != null) {
            setSupportActionBar(this.toolbar);
            if (getSupportActionBar() != null)
                getSupportActionBar().setDisplayShowTitleEnabled(false);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                        WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                if (hasToolbarPadding()) {
                    AppBarLayout appBar = (AppBarLayout) findViewById(getAppBarId());
                    if (appBar != null)
                        appBar.setPadding(0, DisplayUtil.getStatusBarHeight(this), 0, 0);
                }
            }
        }
    }

    /**
     * Set drawable to ActionBar
     *
     * @param iconRes int resource drawable id to set on ActionBar
     */
    public void setActionBarIcon(int iconRes) {
        if (toolbar != null)
            toolbar.setNavigationIcon(iconRes);
    }

    /**
     * Set drawable to ActionBar
     *
     * @param bitmapDrawable BitmapDrawable to set on ActionBar
     */
    public void setActionBarIcon(BitmapDrawable bitmapDrawable) {
        if (toolbar != null)
            toolbar.setNavigationIcon(bitmapDrawable);
    }

    /**
     * Set title to actionBar
     *
     * @param title CharSequence to set on ActionBar
     */
    public void setTitle(CharSequence title) {
        mTitle = title;
        //ImageView appBarIcon = (ImageView) findViewById(getAppBarIconId());
        Label appBarTitle = (Label) findViewById(R.id.title);
        if (appBarTitle != null) { //&& appBarIcon != null) {
            if (title != null) {
                //appBarIcon.setVisibility(View.GONE);
                appBarTitle.setVisibility(View.VISIBLE);
                appBarTitle.setText(title);
            } else {
                //appBarIcon.setVisibility(View.VISIBLE);
                appBarTitle.setVisibility(View.GONE);
            }
        } else if (getSupportActionBar() != null && mTitle != null)
            getSupportActionBar().setTitle(mTitle);
    }


    /**
     * Set Text color title to actionBar
     *
     * @param color int to set on ActionBar
     */
    public void setColorTitle(int color) {
        Label appBarTitle = (Label) findViewById(R.id.title);
        if (appBarTitle != null) { //&& appBarIcon != null) {
            if (color > 0 ) {
                appBarTitle.setTextColor(ContextCompat.getColor(getBaseContext(),color));
            } else {
                //appBarIcon.setVisibility(View.VISIBLE);
                appBarTitle.setVisibility(View.GONE);
            }
        } else if (getSupportActionBar() != null && mTitle != null)
            getSupportActionBar().setTitle(mTitle);
    }

    /**
     * abstract methods
     */
    /**
     * Get toolbar layout id
     *
     * @return R.id.toolbar id if your layout has declared one, otherwise 0
     */
    protected abstract int getToolbarId();

    /**
     * it is used for activity custom toolbar padding in windows status bar translucent feature
     *
     * @return true if you want to setup padding automatically, otherwise false
     */
    protected boolean hasToolbarPadding() {
        return true;
    }

    protected abstract int getAppBarId();

    protected boolean ignoreAuth() {
        return false;
    }
}
