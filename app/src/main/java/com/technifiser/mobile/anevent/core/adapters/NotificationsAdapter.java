package com.technifiser.mobile.anevent.core.adapters;

import android.content.Context;
import android.view.View;

import com.technifiser.framework.uds.api.adapters.CollectionAdapter;
import com.technifiser.mobile.anevent.core.holders.NotificationsHolder;
import com.technifiser.mobile.anevent.core.models.Notifications;
import java.util.List;

/**
 * @author Luis Hernandez
 * @version 0.0.1
 */

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class NotificationsAdapter extends CollectionAdapter<Notifications,NotificationsHolder>{

    private static final String TAG = "NotificationsAdapter";

    /**
     * Default constructor
     *
     * @param context    android Context
     * @param dataSource List<T> Collection to render
     * @param layoutId   int layout resource id to inflate
     */
    public NotificationsAdapter(Context context, List<Notifications> dataSource, int layoutId) {
        super(context, dataSource, layoutId);
    }

    @Override
    protected NotificationsHolder newHolderInstance(View rowView) {
        return new NotificationsHolder(rowView);
    }

    @Override
    protected void bindHolder(NotificationsHolder holder, Notifications item, int position) {
        /*holder.date.setText(item.date);
        holder.name.setText(item.name);
        holder.time.setText(item.time);
        holder.location.setText(item.location);*/
    }
}
