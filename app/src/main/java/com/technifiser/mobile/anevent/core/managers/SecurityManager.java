package com.technifiser.mobile.anevent.core.managers;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.technifiser.framework.uds.api.CommonLogger;
import com.technifiser.framework.uds.api.models.Bearer;
import com.technifiser.framework.uds.api.util.AppSettings;
import com.technifiser.mobile.anevent.R;
import com.technifiser.mobile.anevent.core.models.Session;

/**
 * Security Manager implementation
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */

@SuppressWarnings({"MissingPermission", "deprecation"})
public class SecurityManager {

    private final String TAG = "SecurityManager";
    private Context context;
    private CommonLogger logger;

    public SecurityManager(Context context, CommonLogger logger) {
        this.context = context;
        this.logger = logger;
    }

    public Bearer getBearer() {
        try {
            return  getSession().bearer;
        } catch (ArrayIndexOutOfBoundsException | JsonSyntaxException | NullPointerException e) {
            logger.w(e.getMessage());
        }
        return null;
    }

    /**
     * register a new explicitly account
     *
     * @param username String username or email
     * @param password String password decrypt
     * @param userData Bundle user data with account information to save inside this account configuration
     */
    public void registerAccount(String username, String password, Bundle userData) {
        Account account = new Account(username, context.getString(R.string.account_type));
        AccountManager manager = AccountManager.get(context);
        manager.addAccountExplicitly(account, password, userData);
    }

    /**
     * remove all registered accounts
     */
    public void removeAccounts() {
        AccountManager manager = AccountManager.get(context);
        for (Account account : manager.getAccountsByType(context.getString(R.string.account_type))) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                manager.removeAccountExplicitly(account);
            } else
                manager.removeAccount(account, null, null);
        }
    }


    /**
     * Get saved user account inside the device
     *
     * @return Auth user account object
     */
    public Session getSession() {
        try {
            AccountManager manager = AccountManager.get(context);
            Account account = null;
            if (manager.getAccountsByType(context.getString(R.string.account_type)).length > 0)
                account = manager.getAccountsByType(context.getString(R.string.account_type))[0];
            if (account != null) {
                String accountJson = manager.getUserData(account, AppSettings.AUTH_USER);
                return new Gson().fromJson(accountJson, Session.class);
            }
        } catch (Exception e) {
            logger.setTagName(TAG);
            logger.e(e);
        }
        return null;
    }

    public void updateSession(Session session) {
        if (session == null)
            return;
        try {
            AccountManager manager = AccountManager.get(context);
            Account account = null;
            if (manager.getAccountsByType(context.getString(R.string.account_type)).length > 0)
                account = manager.getAccountsByType(context.getString(R.string.account_type))[0];
            if (account != null) {
                Gson gson = new Gson();
                manager.setUserData(account, AppSettings.AUTH_USER, gson.toJson(session));
            }
        } catch (Exception e) {
            logger.setTagName(TAG);
            logger.e(e);
        }
    }


    /**
     * Check if the user is already authenticated
     *
     * @return true if there are any registered token on wieven account, otherwise false
     */
    public boolean isLogged() {
        try {
            String email = getSession().email;
            if (email == null || email.isEmpty())
                throw new NullPointerException("no token found on this device...");
            else {
                logger.i(String.format("Email found: %s", email));
            }
        } catch (NullPointerException | ArrayIndexOutOfBoundsException e) {
            logger.e(e);
            return false;
        }
        return true;
    }

}
