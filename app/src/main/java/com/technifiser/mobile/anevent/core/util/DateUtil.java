package com.technifiser.mobile.anevent.core.util;

import android.annotation.SuppressLint;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

/**
 * Common date utility class
 *
 * @author Francisco Vásquez
 * @version 1.0
 */
public final class DateUtil {

    public static final String DATE_TIME_SERVER_FORMAT = "yyyy-MM-dd\'T\'HH:mm:ss";

    /**
     * Get String date to show
     *
     * @param _uuid UUID object id
     * @return String date formatted
     */
    public static String getDateFromUUID(String _uuid) {
        UUID uuid = UUID.fromString(_uuid);
        Calendar uuidEpoch = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        uuidEpoch.clear();
        uuidEpoch.set(1582, 9, 15, 0, 0, 0);
        uuidEpoch.setTimeZone(TimeZone.getDefault());
        long epochMillis = uuidEpoch.getTime().getTime();
        long time = (uuid.timestamp() / 10000L) + epochMillis;
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.DEFAULT);
        Date date = new Date(time);
        return dateFormat.format(date);
    }

    /**
     * Get string default date
     *
     * @param timestamp long unix timestamp
     * @return String date
     */
    public static String toStringDate(long timestamp,String formatIn) {
        DateFormat format = new SimpleDateFormat(formatIn, Locale.getDefault());
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.clear();
        calendar.set(1970, 1, 1, 0, 0, 0);
        calendar.setTimeZone(TimeZone.getTimeZone("US/Eastern"));
        calendar.setTimeInMillis(timestamp);
        return format.format(calendar.getTime());
    }

    public static String toShortDateString(Date date) {
        Locale locale = Locale.getDefault();
        DateFormat format = new SimpleDateFormat("MMM dd, yyyy", locale);
        return format.format(date);
    }

    public static String getDate(Date date,String formatOut) {
        Locale locale = new Locale("es", "VE");
        DateFormat format = new SimpleDateFormat(formatOut, locale);
        return format.format(date);
    }

    @SuppressLint("SimpleDateFormat")
    public static String formatUI(String date, String inFormat, String outFormat){
        try {
            return ""+new SimpleDateFormat(outFormat).format(new SimpleDateFormat(inFormat).parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "0001-01-01";
    }

    public static String getDate(String dateIn,String formatOut) {
        DateTime date = ISODateTimeFormat.dateTimeParser().parseDateTime(dateIn);
        Locale locale = new Locale("en", "USA");
        SimpleDateFormat format = new SimpleDateFormat(formatOut, locale);
        return format.format(date.toDate());
    }

    public static String getDateServer(Date time){
        return new SimpleDateFormat(DATE_TIME_SERVER_FORMAT).format(time.getTime());
    }
}
