package com.technifiser.mobile.anevent.core.enums;

/**
 * Custom fonts enumerable
 *
 * @author Francisco Vásquez
 * @version 1.0
 */
public enum FontType {

    OPENSANS_SEMIBOLD("fonts/opensans/OpenSans-Semibold.ttf"),
    OPENSANS_SEMIBOLD_ITALIC("fonts/opensans/OpenSans-SemiboldItalic.ttf"),
    OPENSANS_BOLD("fonts/opensans/OpenSans-Bold.ttf"),
    OPENSANS_BOLD_ITALIC("fonts/opensans/OpenSans-BoldItalic.ttf"),
    OPENSANS_ITALIC("fonts/opensans/OpenSans-Italic.ttf"),
    OPENSANS_LIGHT("fonts/opensans/OpenSans-Light.ttf"),
    OPENSANS_LIGHT_ITALIC("fonts/opensans/OpenSans-LightItalic.ttf"),
    OPENSANS_REGULAR("fonts/opensans/OpenSans-Regular.ttf"),
    OPENSANS_EXTRABOLD("fonts/opensans/OpenSans-ExtraBold.ttf"),
    OPENSANS_EXTRABOLD_ITALIC("fonts/opensans/OpenSans-ExtraBoldItalic.ttf"),
    ICONFONT("fonts/icons/iconos.ttf");

    private String path;

    FontType(String path) {
        this.path = path;
    }

    public String getAssetPath() {
        return path;
    }
}
