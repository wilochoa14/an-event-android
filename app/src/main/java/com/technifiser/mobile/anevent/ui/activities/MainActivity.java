package com.technifiser.mobile.anevent.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;

import com.technifiser.framework.uds.api.fragments.CommonFragment;
import com.technifiser.framework.uds.api.util.AppSettings;
import com.technifiser.mobile.anevent.BuildConfig;
import com.technifiser.mobile.anevent.R;
import com.technifiser.mobile.anevent.abstraction.activities.FirebaseActivity;
import com.technifiser.mobile.anevent.ui.fragments.ConnectFragment;

@SuppressWarnings("FieldCanBeLocal,StaticFieldLeak")
public class MainActivity extends FirebaseActivity{

    TabLayout tabLayout;
    ConnectFragment homeFragment;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setDebuggable(!BuildConfig.BUILD_TYPE.contains("Production"));
        setContentView(R.layout.activity_main);

        homeFragment = new ConnectFragment();
        replaceFragment(homeFragment);

        setToolbar(null);
    }


    public void replaceFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragments, fragment).addToBackStack(null);
        ft.commit();
    }
    @Override
    public boolean hasAccount() {
        /*SecurityManager securityManager = new SecurityManager(this,getLogger());
        return securityManager.isLogged();*/
        return true;
    }

    @Override
    public void showLogin() {
        //AuthenticatorActivity.openForResult(this, AppSettings.REQUEST_LOGIN_CODE);
    }



    @Override
    protected int getToolbarId() {
        return R.id.toolbar;
    }

    @Override
    protected int getAppBarId() {
        return R.id.appbar;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            CommonFragment f = (CommonFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.fragments);
            if (!f.onBackPressed()) {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportFragmentManager().popBackStack();
                } else {
                    if (!(f instanceof ConnectFragment)) {
                        getSupportFragmentManager().popBackStack();
                    } else
                        ActivityCompat.finishAffinity(this);
                }
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AppSettings.REQUEST_LOGIN_CODE) {
            if (resultCode == RESULT_OK) {
                isAuthenticated = true;
                new android.os.Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //initBottonNavigation();
                        //TODO inicializar vista una vez retorne el resultado de la actividad de login
                    }
                }, 300);
            } else {
                // close activity
                ActivityCompat.finishAffinity(this);
            }
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
