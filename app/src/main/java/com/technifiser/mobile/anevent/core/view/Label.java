package com.technifiser.mobile.anevent.core.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

import com.technifiser.mobile.anevent.R;
import com.technifiser.mobile.anevent.core.enums.FontType;

/**
 * Custom Label with more typefaces
 *
 * @author Francisco Vásquez
 * @version 1.0
 */
public class Label extends TextView {

    public Label(Context context) {
        super(context);
    }

    public Label(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode())
            parseAttributes(context, attrs);
    }

    public Label(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode())
            parseAttributes(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public Label(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        if (!isInEditMode())
            parseAttributes(context, attrs);
    }

    /**
     * Parse roboto typeface
     *
     * @param context view context
     * @param attrs   attribute set
     */
    public void parseAttributes(Context context, AttributeSet attrs) {
        try {
            TypedArray values = context.obtainStyledAttributes(attrs, R.styleable.Label);
            FontType robotoType = FontType.values()
                    [values.getInt(R.styleable.Label_fontType, FontType.OPENSANS_REGULAR.ordinal())];
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), robotoType.getAssetPath());
            if (typeface != null) {
                setTypeface(typeface);
            }
            values.recycle();
        } catch (Exception ex) {
            ex.printStackTrace();
            setDefaultTypeface(context);
        }
    }

    /**
     * Set default typeface
     *
     * @param context view context
     */
    public void setDefaultTypeface(Context context) {
        Typeface typeface = Typeface
                .createFromAsset(context.getAssets(), FontType.OPENSANS_REGULAR.getAssetPath());
        if (typeface != null) {
            setTypeface(typeface);
        }
    }

    /**
     * Set custom font to this view
     *
     * @param robotoType FontType to change
     */
    public void setFontType(FontType robotoType) {
        Typeface typeface = Typeface
                .createFromAsset(getContext().getAssets(), robotoType.getAssetPath());
        if (typeface != null) {
            setTypeface(typeface);
        }
    }
}
