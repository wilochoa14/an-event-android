package com.technifiser.mobile.anevent.abstraction.workers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.technifiser.framework.uds.api.CommonLogger;
import com.technifiser.framework.uds.api.activities.CommonActivity;
import com.technifiser.framework.uds.api.interfaces.IBackgroundTask;
import com.technifiser.framework.uds.api.managers.RouteManager;
import com.technifiser.framework.uds.api.models.Bearer;
import com.technifiser.framework.uds.api.worker.CommonBackgroundTask;
import com.technifiser.mobile.anevent.BuildConfig;
import com.technifiser.mobile.anevent.R;
import com.technifiser.mobile.anevent.core.managers.SecurityManager;

/**
 * UDS Pickup Networking Task implementation inherited from Background Task api definition
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */

public abstract class NetworkingTask<T> extends CommonBackgroundTask<T> {

    private SecurityManager securityManager;

    /**
     * BackgroundTask default constructor
     *
     * @param context  Activity Context
     * @param callBack IBackgroundTask implementation
     */
    protected NetworkingTask(@NonNull Context context, @Nullable IBackgroundTask<T> callBack) {
        super(context, callBack);
    }

    @Override
    protected Bearer getBearer() {
        return getSecurityManager().getBearer();
    }

    protected RouteManager getRouterBuilder() {
        return super.getRouterBuilder(R.string.service_base_url);
    }

    protected SecurityManager getSecurityManager() {
        if (securityManager == null)
            securityManager = new SecurityManager(context, getLogger());
        return securityManager;
    }

    @Override
    public CommonLogger getLogger() {
        CommonLogger logger = new CommonLogger(!BuildConfig.BUILD_TYPE.contains("Production"));
        if (context instanceof CommonActivity)
            logger = ((CommonActivity) context).getLogger();
        logger.setTagName(this.getClass().getName());
        return logger;
    }
}
