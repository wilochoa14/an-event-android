package com.technifiser.mobile.anevent.core;

import com.technifiser.framework.uds.api.FirebaseLogger;
import com.technifiser.framework.uds.api.app.CommonApp;

/**
 * UDOG Core Application class
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */

public class Application extends CommonApp {

    private FirebaseLogger analytics;

    @Override
    public void onCreate() {
        super.onCreate();
        analytics = new FirebaseLogger(this);
    }

    public FirebaseLogger getFirebaseLogger() {
        return analytics;
    }
}
