package com.technifiser.mobile.anevent.core.animations;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

/**
 * @author Luis Hernandez
 * @version 0.0.1
 */

public class AnimationAdapters{

    private static boolean delayEnterAnimation;

    public static void runEnterAnimation(View view, int position,boolean animationsLocked,int lastAnimatedPosition){

        if (animationsLocked) return;

        if (position > lastAnimatedPosition) {
            lastAnimatedPosition = position;
            view.setTranslationY(100);
            view.setAlpha(0.f);
            view.animate()
                    .translationY(0).alpha(1.f)
                    .setStartDelay(delayEnterAnimation ? 20 * (position) : 0)
                    .setInterpolator(new DecelerateInterpolator(2.f))
                    .setDuration(300)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            //animationsLocked = true;
                        }
                    })
                    .start();
        }
    }
}
