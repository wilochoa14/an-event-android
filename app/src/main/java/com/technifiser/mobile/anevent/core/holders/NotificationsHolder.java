package com.technifiser.mobile.anevent.core.holders;

import android.view.View;
import com.technifiser.framework.uds.api.holders.Holder;
import com.technifiser.mobile.anevent.R;
import com.technifiser.mobile.anevent.core.view.Label;

/**
 * @author Luis Hernandez
 * @version 0.0.1
 */

public class NotificationsHolder extends Holder{

    public Label date;
    public Label name;
    public Label location;
    public Label time;

    /**
     * Constructor
     *
     * @param itemView View cardView
     */
    public NotificationsHolder(View itemView) {
        super(itemView);
        date = (Label) itemView.findViewById(R.id.date);
        time = (Label) itemView.findViewById(R.id.time);
    }
}
