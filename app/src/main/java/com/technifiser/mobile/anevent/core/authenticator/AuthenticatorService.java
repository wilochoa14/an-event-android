package com.technifiser.mobile.anevent.core.authenticator;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Authenticator Service Configuration Class
 *
 * @version 0.0.0
 */
public class AuthenticatorService extends Service {

    // Fields
    private static final String TAG = "AuthenticatorServiceLog";
    private Authenticator mAuthenticator;

    /**
     * Create authenticator service
     */
    @Override
    public void onCreate() {
        super.onCreate();
        if (Log.isLoggable(TAG, Log.VERBOSE)) {
            Log.v(TAG, "Authentication Service started.");
        }
        mAuthenticator = new Authenticator(this);
    }

    /**
     * Stopped authenticator service
     */
    @Override
    public void onDestroy() {
        if (Log.isLoggable(TAG, Log.VERBOSE)) {
            Log.v(TAG, "Authentication Service stopped.");
        }
        super.onDestroy();
    }

    /**
     * Get the account authenticator
     *
     * @param intent Intent
     * @return returning the AccountAuthenticator binder for intent
     */
    @Override
    public IBinder onBind(Intent intent) {
        if (Log.isLoggable(TAG, Log.VERBOSE)) {
            Log.v(TAG, "Returning the AccountAuthenticator binder for intent");
        }
        return mAuthenticator.getIBinder();
    }
}