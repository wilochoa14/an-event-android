package com.technifiser.mobile.anevent.ui.fragments;

import android.os.Bundle;

import com.technifiser.framework.uds.api.fragments.CommonFragment;
import com.technifiser.mobile.anevent.R;

/**
 * @author Luis Hernandez
 * @version 0.0.1
 */

public class ConnectFragment extends CommonFragment {


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreateView(Bundle savedInstanceState) {
    }


}
