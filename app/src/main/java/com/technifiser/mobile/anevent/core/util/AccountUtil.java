package com.technifiser.mobile.anevent.core.util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;

import com.facebook.login.LoginManager;
import com.technifiser.framework.uds.api.BuildConfig;
import com.technifiser.framework.uds.api.activities.CommonActivity;
import com.technifiser.framework.uds.api.util.AppSettings;
import com.technifiser.mobile.anevent.core.managers.SecurityManager;
import com.technifiser.mobile.anevent.ui.activities.MainActivity;

import java.util.Random;

/**
 * Common Account utilities
 *
 * @author Francisco Vasquez
 * @version 0.1.0
 */
public class AccountUtil {

    /**
     * Logout from current device account
     *
     * @param parent Material Activity running on the device
     */
    public static void logout(final CommonActivity parent) {
        if (parent == null)
            return;
        LoginManager.getInstance().logOut();
        setLoggedOut(parent, true);
        restartApp(new SecurityManager(parent,parent.getLogger()), parent);
    }

    public static void setLoggedOut(CommonActivity parent, boolean isLoggedOut) {
        if (parent == null)
            return;
        PreferenceManager.getDefaultSharedPreferences(parent.getApplicationContext())
                .edit()
                .putBoolean(AppSettings.APP_LOGGED_OUT, isLoggedOut)
                .apply();
    }

    public static boolean isLoggedOut(CommonActivity parent) {
        return PreferenceManager.getDefaultSharedPreferences(parent.getApplicationContext())
                .getBoolean(AppSettings.APP_LOGGED_OUT, false);
    }

    /**
     * Restart application
     *
     * @param manager SecurityManager
     * @param parent  MaterialActivity
     */
    public static void restartApp(SecurityManager manager, CommonActivity parent) {
        if (parent == null)
            return;
        // UnSubscribe from all topics
        // firebase topic news (production environment)
        //FirebaseMessaging.getInstance().unsubscribeFromTopic(parent.getString(R.string.firebase_topic_news));
        // firebase topic news (development environment) only if the build is debug
        if (BuildConfig.DEBUG) {
            //FirebaseMessaging.getInstance().unsubscribeFromTopic(parent.getString(R.string.firebase_topic_news_dev));
        }
        /* deleting all local files */
        PreferenceManager.getDefaultSharedPreferences(parent.getApplicationContext()).edit().clear()
                .apply();
        manager.removeAccounts();
        ActivityCompat.finishAffinity(parent);
        Intent mainActivity = new Intent(parent, MainActivity.class);
        mainActivity.putExtra(AppSettings.ACCOUNT_FORCE_LOGOUT, true);
        /* Indicate that is logged out */
        setLoggedOut(parent, true);
        int pendingIntentId = new Random().nextInt();
        PendingIntent pendingIntent = PendingIntent.getActivity(parent, pendingIntentId,
                mainActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) parent.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1, pendingIntent);
        System.exit(0);
    }

}

