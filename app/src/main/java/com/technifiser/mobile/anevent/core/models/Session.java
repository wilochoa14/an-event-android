package com.technifiser.mobile.anevent.core.models;

import com.google.gson.annotations.SerializedName;
import com.technifiser.framework.uds.api.models.Bearer;

import java.io.Serializable;

/**
 * @author Luis Hernandez
 * @version 0.0.1
 */

public class Session implements Serializable{
    @SerializedName("email")
    public String email;

    public Bearer bearer;
}
