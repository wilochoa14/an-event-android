package com.technifiser.mobile.anevent.core.authenticator;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.technifiser.framework.uds.api.util.AppSettings;
import com.technifiser.mobile.anevent.abstraction.activities.FirebaseLoginActivity;

/**
 * Authenticator class configuration
 *
 * @version 0.0.0
 */
@SuppressWarnings("all")
public class Authenticator extends AbstractAccountAuthenticator {

    // Fields
    private final Context context;

    /**
     * Constructor
     *
     * @param context context of the application
     */
    public Authenticator(Context context) {
        super(context);
        this.context = context;
    }

    /**
     * Add an account
     *
     * @param response         authenticator response (AccountAuthenticatorResponse)
     * @param accountType      authenticator type (String)
     * @param authTokenType    authenticator token type name (String)
     * @param requiredFeatures optional param (String[]) null if not needed
     * @param options          Bundle with data to save on this account (Bundle)
     * @return Bundle of this configuration
     * @throws NetworkErrorException
     */
    @Override
    public Bundle addAccount(AccountAuthenticatorResponse response, String accountType,
                             String authTokenType, String[] requiredFeatures, Bundle options) throws NetworkErrorException {

        final Intent authIntent = new Intent(context, FirebaseLoginActivity.class); //// TODO: 1/10/17 Replace with the real Login Implementation
        authIntent.putExtra(AppSettings.AUTH_TOKEN, authTokenType);
        authIntent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);

        final Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, authIntent);
        return bundle;
    }

    /**
     * Check current account
     *
     * @param response Account to check (AccountAuthenticatorResponse)
     * @param account  Current Account to check (Account)
     * @param options  Bundle with data saved in this account (Bundle)
     * @return Bundle with data
     * @throws NetworkErrorException
     */
    @SuppressWarnings("ConstantConditions")
    @Override
    public Bundle confirmCredentials(AccountAuthenticatorResponse response, Account account, Bundle options) throws NetworkErrorException {
        boolean isValidToken = true;
        AccountManager manager = AccountManager.get(context);
        final String userToken = manager.getUserData(account, AppSettings.AUTH_TOKEN);
        //TODO: Confirm Credentials
        // set response to return confirmation
        Bundle confirmResult = new Bundle();
        confirmResult.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, isValidToken);
        return confirmResult;
    }

    /**
     * Edit the properties of account
     *
     * @param response    AccountAuthenticatorResponse
     * @param accountType String
     * @return Bundle
     */
    @Override
    public Bundle editProperties(AccountAuthenticatorResponse response, String accountType) {
        return null;
    }

    /**
     * Get token of authentication
     *
     * @param response
     * @param account
     * @param authTokenType
     * @param options
     * @return
     * @throws NetworkErrorException
     */
    @Override
    public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) throws NetworkErrorException {
        return null;
    }

    /**
     * Get token of authentication
     *
     * @param authTokenType
     * @return
     */
    @Override
    public String getAuthTokenLabel(String authTokenType) {
        return null;
    }

    /**
     * Updates credentials of account
     *
     * @param response
     * @param account
     * @param authTokenType
     * @param options
     * @return
     * @throws NetworkErrorException
     */
    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) throws NetworkErrorException {
        return null;
    }

    /**
     * Features of account
     *
     * @param response
     * @param account
     * @param features
     * @return
     * @throws NetworkErrorException
     */
    @Override
    public Bundle hasFeatures(AccountAuthenticatorResponse response, Account account, String[] features) throws NetworkErrorException {
        return null;
    }
}