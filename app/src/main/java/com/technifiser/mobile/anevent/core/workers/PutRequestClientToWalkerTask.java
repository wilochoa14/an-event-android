package com.technifiser.mobile.anevent.core.workers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.technifiser.framework.uds.api.enums.RequestMethodType;
import com.technifiser.framework.uds.api.interfaces.IBackgroundTask;
import com.technifiser.mobile.anevent.abstraction.workers.NetworkingTask;

/**
 * @author Luis Hernandez
 * @version 0.0.1
 */

public class PutRequestClientToWalkerTask extends NetworkingTask<Integer>{

    public String typeRequest;
    public String idWalker;
    public String idClient;

    /**
     * BackgroundTask default constructor
     *
     * @param context  Activity Context
     * @param callBack IBackgroundTask implementation
     */
    public PutRequestClientToWalkerTask(@NonNull Context context, @Nullable IBackgroundTask<Integer> callBack) {
        super(context, callBack);
    }

    @Override
    protected Integer doInBackground() throws Exception {
        String route = "";//getRouterBuilder().build(R.string.put_request_client_walks_api,idClient,idWalker,typeRequest);
        return getNetworkManager().doRequest(RequestMethodType.PUT,route,null,Integer.class);
    }

    public void setTypeRequest(String typeRequest) {
        this.typeRequest = typeRequest;
    }

    public void setIdWalker(String idWalker) {
        this.idWalker = idWalker;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }
}
