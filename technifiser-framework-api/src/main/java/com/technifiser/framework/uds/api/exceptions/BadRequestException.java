package com.technifiser.framework.uds.api.exceptions;

/**
 * Handle Http Status Code 403 Forbidden
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */
public class BadRequestException extends Exception {

    public BadRequestException(String response) {
        super(response);
    }
}
