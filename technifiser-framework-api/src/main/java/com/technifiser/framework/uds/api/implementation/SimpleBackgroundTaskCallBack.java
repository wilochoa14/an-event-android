package com.technifiser.framework.uds.api.implementation;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.technifiser.framework.uds.api.CommonLogger;
import com.technifiser.framework.uds.api.interfaces.IBackgroundTask;

import java.util.Locale;

/**
 * Simple background task call back implementation
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */
public class SimpleBackgroundTaskCallBack<T> implements IBackgroundTask<T> {

    private final String TAG = "SimpleWorkerCallBack";
    private CommonLogger logger;

    public SimpleBackgroundTaskCallBack() {
    }

    public SimpleBackgroundTaskCallBack(@NonNull CommonLogger logger) {
        this.logger = logger;
    }

    @Override
    public void runOnUIThread(T entry) {
        if (logger != null) {
            logger.setTagName(TAG);
            logger.i(String.format(Locale.getDefault(), "runOnUIThread: %s", new Gson().toJson(entry)));
        }
    }

    @Override
    public void runOnUIThread(Throwable ex) {
        if (logger != null) {
            logger.setTagName(TAG);
            logger.e(ex);
        }
    }

    @Override
    public void runOnBackground(T entry) {
        if (logger != null) {
            logger.setTagName(TAG);
            logger.i(String.format(Locale.getDefault(), "runOnBackground: %s", new Gson().toJson(entry)));
        }
    }

    @Override
    public void runOnBackground(Throwable ex) {
        if (logger != null) {
            logger.setTagName(TAG);
            logger.e(ex);
        }
    }
}
