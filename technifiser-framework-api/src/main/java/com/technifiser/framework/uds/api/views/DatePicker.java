package com.technifiser.framework.uds.api.views;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;

import java.util.Calendar;
import java.util.Date;

/**
 * Common Date Picker Dialog Fragment Definition
 *
 * @author Francisco Vasquez
 * @version 0.0.1
 */
public class DatePicker extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private OnDateSelected onDateSelected;
    private Date initial;

    public static void show(FragmentManager fManager, OnDateSelected onDateSelected, @Nullable Date initial) {
        if (fManager == null || onDateSelected == null)
            return;
        DatePicker dp = new DatePicker();
        dp.setOnDateSelected(onDateSelected);
        dp.setInitial(initial);
        dp.show(fManager, "datePicker");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        if (initial != null)
            c.setTime(initial);
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(android.widget.DatePicker datePicker, int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(year, month, day);
        if (onDateSelected != null)
            onDateSelected.onDateSet(c.getTime());
    }

    public void setOnDateSelected(OnDateSelected onDateSelected) {
        this.onDateSelected = onDateSelected;
    }

    public void setInitial(Date initial) {
        this.initial = initial;
    }

    public interface OnDateSelected {

        void onDateSet(Date dateSelected);
    }
}
