package com.technifiser.framework.uds.api.util;

import android.content.Context;
import android.os.Build;
import android.preference.PreferenceManager;

import com.squareup.okhttp.MediaType;

import java.util.Locale;
import java.util.UUID;

/**
 * Common static resources
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */
public final class AppSettings {

    // Media type constants
    public static final MediaType MEDIA_JSON_TYPE = MediaType.parse("application/json; charset=utf-8");
    public static final MediaType MEDIA_FROM_URLENCODED_TYPE = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");

    // String constants
    public static final String AUTH_TOKEN = "com.technifiser.framework.api.account.data:token";
    public static final String AUTH_USER = "com.technifiser.framework.api.account.data:user";
    public static final String AUTH_BEARER = "com.technifiser.framework.api.account.data:bearer";
    public static final String AUTH_EMAIL_ADDRESS = "com.technifiser.framework.api.account:email";
    public static final String APP_LOGGED_OUT = "com.technifiser.framework.api.account:logout";
    public static final String ACCOUNT_FORCE_LOGOUT = "com.technifiser.framework.api.account:logout_forced";
    public static final String FIREBASE_NEW_USER_KEY = "new_registered_user";
    public static final String FIREBASE_EMAIL_KEY = "email";
    // integer constants
    public static final int REQUEST_AUTHENTICATOR = 100;
    public static final int REQUEST_LOGIN_CODE = 1;
    public static final int MARKETING_PAGES = 3;
    public static final int REQUEST_SETTINGS_CONFIGURATIONS = 2;
    public static final int REQUEST_LOCATION_PERMISSION_GROUP = 4;
    public static final int REQUEST_CALL_PERMISSION_GROUP = 5;
    public static final int REQUEST_READ_EXTERNAL_STORAGE_PERMISSION_GROUP = 5;

    private static final String DEVICE_ID = "com.technifiser.framework.api.device:id";


    /**
     * String unique device identification
     *
     * @param context Context
     * @return String device id
     */
    public static String deviceId(Context context) {
        String deviceId = PreferenceManager.getDefaultSharedPreferences(context)
                .getString(DEVICE_ID, null);
        if (deviceId == null || deviceId.isEmpty()) {
            deviceId = String.format(Locale.getDefault(), "%s_%s", getDevice(), UUID.randomUUID().toString());
            PreferenceManager.getDefaultSharedPreferences(context)
                    .edit().putString(DEVICE_ID, deviceId).apply();
        }
        return deviceId;
    }

    /**
     * Get Device Name
     *
     * @return String device name
     */
    public static String getDevice() {
        return Build.DEVICE;
    }

    /**
     * Get Default Language
     *
     * @return String default Language
     */
    public static String getLanguage() {
        return Locale.getDefault().getLanguage();
    }
}
