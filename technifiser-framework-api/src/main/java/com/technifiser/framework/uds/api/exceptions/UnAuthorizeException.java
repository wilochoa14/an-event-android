package com.technifiser.framework.uds.api.exceptions;

/**
 * UnAuthorize Exception Class
 * Authorize one exception
 *
 * @author Francisco Vásquez
 * @version 0.0.0
 */
@SuppressWarnings("FieldCanBeLocal")
public class UnAuthorizeException extends Exception {

    public UnAuthorizeException(String response) {
        super(response);
    }
}
