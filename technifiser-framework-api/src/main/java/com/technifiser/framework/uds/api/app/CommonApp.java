package com.technifiser.framework.uds.api.app;

import android.support.multidex.MultiDexApplication;

import com.technifiser.framework.uds.api.CommonLogger;

/**
 * Common Multidex application
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */

public class CommonApp extends MultiDexApplication {

    protected CommonLogger logger;


    public CommonLogger getLogger(boolean debuggable) {
        if (logger == null)
            logger = new CommonLogger(debuggable);
        logger.setDebugConfig(debuggable);
        return logger;
    }
}
