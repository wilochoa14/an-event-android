package com.technifiser.framework.uds.api.interfaces;

/**
 * Background Task interface
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */

public interface IBackgroundTask<T> {

    void runOnUIThread(T entry);

    void runOnUIThread(Throwable ex);

    void runOnBackground(T entry);

    void runOnBackground(Throwable ex);
}
