package com.technifiser.framework.uds.api.exceptions;

/**
 * Http not found exception
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */
public class NotFoundException extends Exception {

    public NotFoundException(String response) {
        super(response);
    }

}
