package com.technifiser.framework.uds.api.exceptions;

/**
 * Handle Http Status Code 403 Forbidden
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */
public class ForbiddenException extends Exception {

    public ForbiddenException(String response) {
        super(response);
    }
}
