package com.technifiser.framework.uds.api.interfaces;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import java.util.List;

/**
 * Broadcast Channel Interface
 * Implement this interface if you want to subscribe for local broadcast messages
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */

public interface IBroadcastChannel {

    /**
     * @return Return a List of IntentFilter to subscribe for local broadcast messages
     */
    List<IntentFilter> getChannels();

    /**
     * It called when a message is received from any intent filter passed
     *
     * @param context Context
     * @param data    Intent
     */
    void onMessageReceived(Context context, Intent data);
}
