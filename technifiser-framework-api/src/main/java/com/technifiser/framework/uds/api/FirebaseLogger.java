package com.technifiser.framework.uds.api;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;

import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Firebase Logger definition for common usage
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */

@SuppressWarnings("MissingPermission")
public class FirebaseLogger {

    private Context context;
    private FirebaseAnalytics firebaseAnalytics;

    public FirebaseLogger(@NonNull Context context) {
        this.context = context;
    }

    @SuppressLint("HardwareIds")
    public void reportAppOpen() {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID,
                Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
        // Send log to Firebase console
        getInstance().logEvent(FirebaseAnalytics.Event.APP_OPEN, bundle);
    }

    public FirebaseAnalytics getInstance() {
        if (firebaseAnalytics == null)
            firebaseAnalytics = FirebaseAnalytics.getInstance(context);
        return firebaseAnalytics;
    }

}
