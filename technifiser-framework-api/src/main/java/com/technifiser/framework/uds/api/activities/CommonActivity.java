package com.technifiser.framework.uds.api.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.technifiser.framework.uds.api.CommonLogger;
import com.technifiser.framework.uds.api.app.CommonApp;
import com.technifiser.framework.uds.api.interfaces.IBroadcastChannel;

/**
 * Common base activity
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */
public abstract class CommonActivity extends AppCompatActivity {

    /**
     * BroadcastReceiver
     */
    private BroadcastReceiver localReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ((IBroadcastChannel) CommonActivity.this).onMessageReceived(context, intent);
        }
    };

    /**
     * FLAGS
     */
    private boolean debuggable = false;


    @SuppressWarnings("UnnecessaryReturnStatement")
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (!hasAccount()) {
            showLogin();
            return;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerForReceiver();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!hasAccount()) {
            showLogin();
            return;
        }
        registerForReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (this instanceof IBroadcastChannel) {
            getLogger().i("UnRegistering for local receiver");
            LocalBroadcastManager.getInstance(this).unregisterReceiver(localReceiver);
        }
    }

    @Override
    public void finish() {
        hideKeyword();
        super.finish();
    }

    protected void registerForReceiver() {
        if (!(this instanceof IBroadcastChannel))
            return;
        for (IntentFilter channel : ((IBroadcastChannel) this).getChannels()) {
            getLogger().i("Registering for broadcast receiver " + channel.getAction(0));
            LocalBroadcastManager.getInstance(this)
                    .registerReceiver(localReceiver, channel);
        }
    }

    public void setStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(color);
        }
    }

    /**
     * Get common logger instance to log any message or exception thrown in the application
     *
     * @return CommonLogger instance object
     */
    public CommonLogger getLogger() {
        CommonLogger logger = new CommonLogger(debuggable);
        if (this.getApplicationContext() instanceof CommonApp) {
            logger = ((CommonApp) this.getApplicationContext()).getLogger(debuggable);
        }
        logger.setTagName(this.getClass().getName());
        return logger;
    }

    /**
     * Hide android keyword when needed by getting the active current focus
     */
    public void hideKeyword() {
        try {
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm.isActive())
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Turn on / off debug access
     *
     * @param debuggable true if the Build Configuration is debuggable, otherwise false
     */
    public void setDebuggable(boolean debuggable) {
        this.debuggable = debuggable;
    }


    public abstract boolean hasAccount();

    public abstract void showLogin();
}
