package com.technifiser.framework.uds.api.exceptions;

/**
 * Handle HttpStatus Code 500 Internal Server Error
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */
public class ServerErrorException extends Exception {

    public ServerErrorException(String response) {
        super(response);
    }
}
