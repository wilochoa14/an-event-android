package com.technifiser.framework.uds.api.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Bearer serialized tokens
 *
 * @author Francisco Vásquez
 * @version 0.0.0
 */

public class Bearer implements Serializable {
    @SerializedName("access_token")
    public String accessToken;
    @SerializedName("token_type")
    public String tokenType;
    @SerializedName("expires_in")
    public long expiresIn;
    @SerializedName("refresh_token")
    public String refreshToken;

    //No-Maped
    public String typeAuthorization;
}
