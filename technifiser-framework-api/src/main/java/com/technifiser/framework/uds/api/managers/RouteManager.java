package com.technifiser.framework.uds.api.managers;

import android.content.Context;


import com.technifiser.framework.uds.api.CommonLogger;

import java.util.Locale;

/**
 * route manager utility class
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */

public class RouteManager {

    private Context context;
    private int hostId;

    public RouteManager(Context context) {
        this.context = context;
    }

    /**
     * Build a string route
     *
     * @param routeId int resource id
     * @param params  Object... arguments
     * @return String formatted
     */
    public String build(int routeId, Object... params) {
        try {
            return String.format(Locale.getDefault(), context.getString(hostId) + context.getString(routeId), params);
        } catch (Exception e) {
            new CommonLogger().e(e);
        }
        return "";
    }

    public void setHostId(int hostId) {
        this.hostId = hostId;
    }
}
