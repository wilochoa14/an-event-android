package com.technifiser.framework.uds.api.activities;

import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.technifiser.framework.uds.api.interfaces.ILogin;

/**
 * Keylimetie Login Activity
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */
public abstract class CommonLoginActivity<T> extends CommonActivity
        implements ILogin<T> {

    private AccountAuthenticatorResponse mAccountAuthenticatorResponse;
    private Bundle mAccountResultBundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /* setting up authenticator response */
        bindAuthenticationResponse();
    }

    @Override
    public void finish() {
        setEntryResponse();
        super.finish();
    }

    @Override
    public void bindAuthenticationResponse() {
        getLogger().i("setting up authentication response");
        mAccountAuthenticatorResponse =
                getIntent().getParcelableExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE);
        if (mAccountAuthenticatorResponse != null) {
            mAccountAuthenticatorResponse.onRequestContinued();
        }
    }

    @Override
    public void setAccountAuthenticatorResult(Bundle result) {
        getLogger().i("setting up account bundle result");
        mAccountResultBundle = result;
    }

    @Override
    public void setEntryResponse() {
        if (mAccountAuthenticatorResponse != null) {
            // send the result bundle back if set, otherwise send an error.
            if (mAccountResultBundle != null) {
                mAccountAuthenticatorResponse.onResult(mAccountResultBundle);
            } else {
                mAccountAuthenticatorResponse.onError(AccountManager.ERROR_CODE_CANCELED,
                        "canceled");
            }
            mAccountAuthenticatorResponse = null;
        }
    }

    @Override
    public void setAccountEntry(T entry, String password) {
        getLogger().i("setting up account entry result");
    }

    @Override
    public boolean hasAccount() {
        return false;
    }

    @Override
    public void showLogin() {
        // do nothing
    }
}
