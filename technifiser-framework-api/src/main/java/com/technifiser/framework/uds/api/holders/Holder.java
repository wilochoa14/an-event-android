package com.technifiser.framework.uds.api.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * View Holder base class
 *
 * @author Francisco Vasquez
 * @version 1.0
 */
public abstract class Holder extends RecyclerView.ViewHolder {

    /**
     * Constructor
     *
     * @param itemView View cardView
     */
    public Holder(View itemView) {
        super(itemView);
    }
}
