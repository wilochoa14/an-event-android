package com.technifiser.framework.uds.api.exceptions;

/**
 * Handle Http Status Code 422 Custom Wieven Exception
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */
@SuppressWarnings("FieldCanBeLocal")
public class ValidationException extends Exception {

    public ValidationException(String response) {
        super(response);
    }
}
