package com.technifiser.framework.uds.api.views;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.technifiser.framework.uds.api.enums.AssetFontType;


/**
 * Custom Tab layout view item where you can define a custom roboto typefaces for each tab
 *
 * @author Francisco Vasquez
 * @version 0.0.1
 */
public class TabLayoutView extends TabLayout {

    private static final String TAG = "TabLayout";

    public TabLayoutView(Context context) {
        super(context);
        if (!isInEditMode())
            setAssetFontType(AssetFontType.getDefault());
    }

    public TabLayoutView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode())
            setAssetFontType(AssetFontType.getDefault());
    }

    public TabLayoutView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode())
            setAssetFontType(AssetFontType.getDefault());
    }

    /**
     * Set custom typeface for each child view on each tab
     *
     * @param fontType typeface to set up.
     */
    public void setAssetFontType(AssetFontType fontType) {
        try {
            Typeface typeface = Typeface.createFromAsset(getContext().getAssets(),
                    fontType.getAssetPath());
            for (int position = 0; position < getTabCount(); position++) {
                ViewGroup tab = (ViewGroup) getChildAt(position);
                if (tab != null) {
                    for (int childPosition = 0; childPosition < tab.getChildCount(); childPosition++) {
                        ViewGroup tabViewChild = (ViewGroup) tab.getChildAt(childPosition);
                        for (int x = 0; x < tabViewChild.getChildCount(); x++) {
                            View view = tabViewChild.getChildAt(x);
                            if (view instanceof TextView) {
                                ((TextView) view).setPadding(4, 4, 4, 4);
                                ((TextView) view).setTypeface(typeface);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage(), ex);
        }
    }
}
