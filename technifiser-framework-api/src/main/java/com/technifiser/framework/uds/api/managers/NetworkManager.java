package com.technifiser.framework.uds.api.managers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.technifiser.framework.uds.api.CommonLogger;
import com.technifiser.framework.uds.api.activities.CommonActivity;
import com.technifiser.framework.uds.api.enums.RequestMethodType;
import com.technifiser.framework.uds.api.exceptions.BadRequestException;
import com.technifiser.framework.uds.api.exceptions.ForbiddenException;
import com.technifiser.framework.uds.api.exceptions.InternetAccessException;
import com.technifiser.framework.uds.api.exceptions.NotFoundException;
import com.technifiser.framework.uds.api.exceptions.NotImplementedException;
import com.technifiser.framework.uds.api.exceptions.ServerErrorException;
import com.technifiser.framework.uds.api.exceptions.UnAuthorizeException;
import com.technifiser.framework.uds.api.exceptions.ValidationException;
import com.technifiser.framework.uds.api.models.Bearer;
import com.technifiser.framework.uds.api.util.AppSettings;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Locale;

/**
 * Network Manager common utility class
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */
@SuppressWarnings({"WeakerAccess", "unchecked", "StringBufferReplaceableByString"})
public class NetworkManager {

    private final OkHttpClient client = new OkHttpClient();
    private Context context;

    private Bearer bearer;

    public NetworkManager(Context context) {
        this.context = context;
    }

    public Bearer getBearer() {
        return bearer;
    }

    public Context getContext() {
        return context;
    }

    public <T> T doRequest(RequestMethodType method, String endpoint, @Nullable String jsonBody, Class<T> classType) throws Exception {
        Response response = connect(method, endpoint, AppSettings.MEDIA_JSON_TYPE, jsonBody);
        return handleResponse(response.statusCode, response.io, response.err, classType);
    }

    public <T> List<T> doRequest(RequestMethodType method, String endpoint, @Nullable String jsonBody, Type classType) throws Exception {
        Response response = connect(method, endpoint, AppSettings.MEDIA_JSON_TYPE, jsonBody);
        return handleResponse(response.statusCode, response.io, response.err, classType);
    }

    protected Response connect(RequestMethodType method, String endpoint, @NonNull MediaType mediaType, @Nullable String bodyContent) throws Exception {
        Response response = new Response();
        getLogger().i(String.format("%s: %s", method, endpoint));
        if (!hasInternetConnection())
            throw new InternetAccessException(getActiveNetworkInfo(), "no internet connection");

        RequestBody requestBody = bodyContent != null && !bodyContent.isEmpty() ? RequestBody.create(mediaType, bodyContent) : null;
        Request.Builder requestBuilder = new Request.Builder()
                .url(endpoint)
                .method(method.method, requestBody);

        if (bearer != null && bearer.accessToken != null && !bearer.accessToken.isEmpty()) {
            getLogger().i(String.format("Authorization: %s %s", bearer.accessToken,bearer.typeAuthorization));
            if (bearer.typeAuthorization == null)
                requestBuilder.addHeader("Authorization", String.format(Locale.getDefault(), "%s %s", bearer.tokenType, bearer.accessToken));
            else
                requestBuilder.addHeader(bearer.typeAuthorization, String.format(Locale.getDefault(), "%s %s", bearer.tokenType, bearer.accessToken));

            getLogger().w(requestBuilder.build().toString());
        }
        com.squareup.okhttp.Response result = client.newCall(requestBuilder.build()).execute();
        int responseCode = result.code();
        if (responseCode < 400)
            response.io = result.body().byteStream();
        else
            response.err = result.body().byteStream();
        response.statusCode = responseCode;
        return response;
    }

    private <T> T handleResponse(int statusCode, InputStream io, InputStream err, Class<T> classType) throws Exception {
        switch (statusCode) {
            case HttpURLConnection.HTTP_ACCEPTED:
            case HttpURLConnection.HTTP_OK:
            case HttpURLConnection.HTTP_CREATED:
            case HttpURLConnection.HTTP_NO_CONTENT:
            case HttpURLConnection.HTTP_NOT_AUTHORITATIVE:
                if (classType == Integer.class) {
                    return (T) (Integer) statusCode;
                }
                String bodyString = IOUtils.toString(io, "utf-8");
                return new Gson().fromJson(bodyString, classType);
            case HttpURLConnection.HTTP_BAD_REQUEST:
                String message = IOUtils.toString(err, "utf-8");
                throw new BadRequestException(message);
            case 422:
                message = IOUtils.toString(err, "utf-8");
                throw new ValidationException(message);
            case HttpURLConnection.HTTP_FORBIDDEN:
                message = IOUtils.toString(err, "utf-8");
                throw new ForbiddenException(message);
            case HttpURLConnection.HTTP_NOT_IMPLEMENTED:
                message = IOUtils.toString(err, "utf-8");
                throw new NotImplementedException(message);
            case HttpURLConnection.HTTP_INTERNAL_ERROR:
                message = IOUtils.toString(err, "utf-8");
                throw new ServerErrorException(message);
            case HttpURLConnection.HTTP_NOT_FOUND:
                message = IOUtils.toString(err, "utf-8");
                throw new NotFoundException(message);
            case HttpURLConnection.HTTP_UNAUTHORIZED:
                message = IOUtils.toString(err, "utf-8");
                throw new UnAuthorizeException(message);
            default:
                message = IOUtils.toString(err, "utf-8");
                throw new IOException(message);
        }
    }

    private <T> List<T> handleResponse(int statusCode, InputStream io, InputStream err, Type modelType) throws Exception {
        switch (statusCode) {
            case HttpURLConnection.HTTP_ACCEPTED:
            case HttpURLConnection.HTTP_OK:
            case HttpURLConnection.HTTP_CREATED:
            case HttpURLConnection.HTTP_NO_CONTENT:
            case HttpURLConnection.HTTP_NOT_AUTHORITATIVE:
                String bodyString = IOUtils.toString(io, "utf-8");
                return (List<T>) new Gson().fromJson(bodyString, modelType);
            case HttpURLConnection.HTTP_BAD_REQUEST:
                String message = IOUtils.toString(err, "utf-8");
                throw new BadRequestException(message);
            case 422:
                message = IOUtils.toString(err, "utf-8");
                throw new ValidationException(message);
            case HttpURLConnection.HTTP_FORBIDDEN:
                message = IOUtils.toString(err, "utf-8");
                throw new ForbiddenException(message);
            case HttpURLConnection.HTTP_NOT_IMPLEMENTED:
                message = IOUtils.toString(err, "utf-8");
                throw new NotImplementedException(message);
            case HttpURLConnection.HTTP_INTERNAL_ERROR:
                message = IOUtils.toString(err, "utf-8");
                throw new ServerErrorException(message);
            case HttpURLConnection.HTTP_NOT_FOUND:
                message = IOUtils.toString(err, "utf-8");
                throw new NotFoundException(message);
            case HttpURLConnection.HTTP_UNAUTHORIZED:
                message = IOUtils.toString(err, "utf-8");
                throw new UnAuthorizeException(message);
            default:
                throw new IOException();
        }
    }

    public synchronized Bearer requestToken(@NonNull String tokenEndpoint, @NonNull String username, @NonNull String password) throws Exception {
        URL endpoint = new URL(tokenEndpoint);
        StringBuilder sb = new StringBuilder("grant_type=password")
                .append("&username=").append(username)
                .append("&password=").append(password);
        Response response = connect(RequestMethodType.POST, endpoint.toString(), AppSettings.MEDIA_FROM_URLENCODED_TYPE, sb.toString());
        return handleResponse(response.statusCode, response.io, response.err, Bearer.class);
    }

    public synchronized Bearer refreshToken(@NonNull String tokenEndpoint, @NonNull Bearer bearer) throws NotImplementedException {
        throw new NotImplementedException(""); //// TODO: 1/10/17 Implement this method
    }

    /**
     * Get Common Logger instance object
     *
     * @return CommonLogger
     */
    private CommonLogger getLogger() {
        CommonLogger logger;
        if (context instanceof CommonActivity)
            logger = ((CommonActivity) context).getLogger();
        else
            logger = new CommonLogger();
        logger.setTagName(this.getClass().getName());
        return logger;
    }

    /**
     * Check if the active network info has internet connection
     *
     * @return true if the active network info has internet connection, otherwise false
     */

    public boolean hasInternetConnection() {
        try {
            return getActiveNetworkInfo().isConnected();
        } catch (Exception ex) {
            getLogger().e(ex);
        }
        return false;
    }

    /**
     * Get active network info
     *
     * @return NetworkInfo instance object
     */
    public NetworkInfo getActiveNetworkInfo() throws NullPointerException {
        ConnectivityManager conn = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return conn.getActiveNetworkInfo();
    }

    public void setBearer(Bearer bearer) {
        this.bearer = bearer;
    }

    @SuppressWarnings("WeakerAccess")
    public class Response {
        public InputStream io;
        public InputStream err;
        public int statusCode;
    }

}
