package com.technifiser.framework.uds.api.interfaces;

import android.os.Bundle;

/**
 * Login interface
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */
public interface ILogin<T> {
    /**
     * Load Authenticator Response
     */
    void bindAuthenticationResponse();

    /**
     * Set account Authentication result
     */
    void setAccountAuthenticatorResult(Bundle result);

    /**
     * finish authentication response and set results before finish the activity
     */
    void setEntryResponse();

    /**
     * Finish authentication and set result on this function
     */
    void setAccountEntry(T entry, String password);
}
