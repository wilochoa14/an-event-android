package com.technifiser.framework.uds.api.enums;

/**
 * Request Method Enumerable Type definition
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */

public enum RequestMethodType {

    GET("GET"),
    POST("POST"),
    PUT("PUT"),
    PATCH("PATCH"),
    DELETE("DELETE");

    public String method;

    RequestMethodType(String method) {
        this.method = method;
    }
}
