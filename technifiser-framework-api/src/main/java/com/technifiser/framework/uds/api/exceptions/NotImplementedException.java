package com.technifiser.framework.uds.api.exceptions;

/**
 * Handle Http Status code 501 NotImplemented
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */
public class NotImplementedException extends Exception {

    public NotImplementedException(String response) {
        super(response);
    }
}
