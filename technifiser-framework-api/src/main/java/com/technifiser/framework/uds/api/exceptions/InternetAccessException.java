package com.technifiser.framework.uds.api.exceptions;

import android.net.NetworkInfo;
import android.util.Log;

import java.util.Locale;

/**
 * Internet Access Exception
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */
@SuppressWarnings("FieldCanBeLocal")
public class InternetAccessException extends Exception {

    private final String TAG = "InternetAccessException";
    private NetworkInfo _netInfo;

    public InternetAccessException(NetworkInfo netInfo, String response) {
        super(response);
        _netInfo = netInfo;
    }

    @Override
    public void printStackTrace() {
        if (_netInfo != null && _netInfo.getTypeName() != null)
            Log.e(TAG, String.format(Locale.getDefault(), "Network Type %s is not connected", _netInfo.getTypeName()));
        super.printStackTrace();
    }
}
