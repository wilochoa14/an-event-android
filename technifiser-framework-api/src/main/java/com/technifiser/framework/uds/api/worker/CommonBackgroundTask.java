package com.technifiser.framework.uds.api.worker;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.technifiser.framework.uds.api.CommonLogger;
import com.technifiser.framework.uds.api.activities.CommonActivity;
import com.technifiser.framework.uds.api.interfaces.IBackgroundTask;
import com.technifiser.framework.uds.api.managers.NetworkManager;
import com.technifiser.framework.uds.api.managers.RouteManager;
import com.technifiser.framework.uds.api.models.Bearer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Common Background Task common Implementation
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */
@SuppressWarnings("WeakerAccess")
public abstract class CommonBackgroundTask<T> extends Thread {

    protected Context context;
    protected IBackgroundTask<T> callBack;

    /**
     * CommonBackgroundTask default constructor
     *
     * @param context  Activity Context
     * @param callBack IBackgroundTask implementation
     */
    protected CommonBackgroundTask(@NonNull Context context, @Nullable IBackgroundTask<T> callBack) {
        this.context = context;
        this.callBack = callBack;
    }

    @Override
    public void run() {
        try {
            getLogger().i("Background worker has started...");
            notifyUI(doInBackground());
        } catch (final Exception ex) {
            getLogger().e(ex);
            notifyUI(ex);
        } finally {
            getLogger().i("Background worker has finished....");
        }
    }

    @SuppressWarnings("unchecked")
    private void notifyUI(final Object result) {
        if (context == null || callBack == null || result == null)
            return;
        if (result instanceof Exception)
            callBack.runOnBackground((Exception) result);
        else
            callBack.runOnBackground((T) result);
        if (!(context instanceof Activity))
            return;
        ((Activity) context).runOnUiThread(new Runnable() {
            @SuppressWarnings("unchecked")
            @Override
            public void run() {
                if (result instanceof Exception)
                    callBack.runOnUIThread((Exception) result);
                else
                    callBack.runOnUIThread((T) result);
            }
        });
    }

    /**
     * Get networking manager instance object
     *
     * @return NetworkManager instance
     */
    protected NetworkManager getNetworkManager() {
        if (getBearer() != null){
            NetworkManager networkManager = new NetworkManager(context);
            networkManager.setBearer(getBearer());
            return networkManager;
        }
        return  new NetworkManager(context);
    }

    protected RouteManager getRouterBuilder(int hostId) {
        RouteManager routeManager = new RouteManager(context);
        routeManager.setHostId(hostId);
        return routeManager;
    }

    protected abstract T doInBackground() throws Exception;

    protected abstract Bearer getBearer();

    /**
     * Execute this background task with a single thread executor
     *
     * @return ExecutorService where will be running this thread
     */
    public ExecutorService execute() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(this);
        return executor;
    }

    /**
     * Execute this thread into a specific thread pool executor given
     *
     * @param executor ExecutorService where will be running this thread instance
     */
    public void execute(@NonNull ExecutorService executor) {
        executor.execute(this);
    }

    /**
     * Get Logger instance
     *
     * @return CommonLogger instance
     */
    public CommonLogger getLogger() {
        CommonLogger logger = new CommonLogger();
        if (context instanceof CommonActivity)
            logger = ((CommonActivity) context).getLogger();
        logger.setTagName(this.getClass().getName());
        return logger;
    }

}
