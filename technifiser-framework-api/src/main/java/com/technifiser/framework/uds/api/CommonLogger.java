package com.technifiser.framework.uds.api;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

/**
 * Common logger
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */
public class CommonLogger {

    private boolean debugConfig;
    private String tagName = "CommonLogger";

    public CommonLogger(boolean debugConfig) {
        setDebugConfig(debugConfig);
    }

    public CommonLogger() {
        setDebugConfig(BuildConfig.DEBUG);
    }

    public void d(@NonNull String message) {
        if (debugConfig) {
            Log.d(tagName, message);
        }
    }

    public void i(@NonNull String message) {
        if (debugConfig) {
            Log.i(tagName, message);
        }
    }

    public void e(@NonNull String message, @NonNull Throwable exception) {
        FirebaseCrash.report(exception);
        if (debugConfig) {
            Log.e(tagName, message, exception);
        }
    }

    public void e(@NonNull String message) {
        if (debugConfig) {
            Log.e(tagName, message);
        }
    }

    public void w(@NonNull String message) {
        if (debugConfig) {
            Log.w(tagName, message);
        }
    }

    public void e(@NonNull Throwable exception) {
        FirebaseCrash.report(exception);
        try {
            if (debugConfig) {
                Log.e(tagName, exception.getMessage(), exception);
            }
        } catch (Exception e) {
            // ignored
        }
    }


    public void setDebugConfig(boolean debugConfig) {
        this.debugConfig = debugConfig;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
}
