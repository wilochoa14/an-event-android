package com.technifiser.framework.uds.api.enums;

import android.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * Asset font path enumerable definition
 *
 * @author Francisco Vasquez
 * @version 0.0.0
 */
public enum AssetFontType {

    OPENSANS_SEMIBOLD("fonts/opensans/OpenSans-Semibold.ttf"),
    OPENSANS_SEMIBOLD_ITALIC("fonts/opensans/OpenSans-SemiboldItalic.ttf"),
    OPENSANS_BOLD("fonts/opensans/OpenSans-Bold.ttf"),
    OPENSANS_BOLD_ITALIC("fonts/opensans/OpenSans-BoldItalic.ttf"),
    OPENSANS_ITALIC("fonts/opensans/OpenSans-Italic.ttf"),
    OPENSANS_LIGHT("fonts/opensans/OpenSans-Light.ttf"),
    OPENSANS_LIGHT_ITALIC("fonts/opensans/OpenSans-LightItalic.ttf"),
    OPENSANS_REGULAR("fonts/opensans/OpenSans-Regular.ttf"),
    OPENSANS_EXTRABOLD("fonts/opensans/OpenSans-ExtraBold.ttf"),
    OPENSANS_EXTRABOLD_ITALIC("fonts/opensans/OpenSans-ExtraBoldItalic.ttf"),
    ICONFONT("fonts/icons/iconos.ttf");


    private static Map<AssetFontType, String> assets;

    static {
        assets = new HashMap<>();
        assets.put(OPENSANS_SEMIBOLD, OPENSANS_SEMIBOLD.path);
        assets.put(OPENSANS_SEMIBOLD_ITALIC, OPENSANS_SEMIBOLD_ITALIC.path);
        assets.put(OPENSANS_BOLD, OPENSANS_BOLD.path);
        assets.put(OPENSANS_BOLD_ITALIC, OPENSANS_BOLD_ITALIC.path);
        assets.put(OPENSANS_ITALIC, OPENSANS_ITALIC.path);
        assets.put(OPENSANS_LIGHT, OPENSANS_LIGHT.path);
        assets.put(OPENSANS_LIGHT_ITALIC, OPENSANS_LIGHT_ITALIC.path);
        assets.put(OPENSANS_REGULAR, OPENSANS_REGULAR.path);
        assets.put(OPENSANS_EXTRABOLD, OPENSANS_EXTRABOLD.path);
        assets.put(OPENSANS_EXTRABOLD_ITALIC, OPENSANS_EXTRABOLD_ITALIC.path);
        assets.put(ICONFONT, ICONFONT.path);

    }

    private final String path;

    AssetFontType(String path) {
        this.path = path;
    }

    public static void setAssetsPath(Map<AssetFontType, String> _assets) {
        if (_assets == null)
            return;
        for (Map.Entry<AssetFontType, String> entry : _assets.entrySet()) {
            try {
                assets.put(entry.getKey(), entry.getValue());
            } catch (Exception e) {
                Log.i("AssetFonType", e.getMessage());
            }
        }
    }

    public static AssetFontType getDefault() {
        return OPENSANS_REGULAR;
    }

    /**
     * Get Asset Font Path
     *
     * @return String .ttf or .otf path
     */
    public String getAssetPath() {
        return assets.get(this);
    }
}
